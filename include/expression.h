/*
 * tokens.h
 *	created by Douglas Braz, Graco Babeuf, Juliano Cardoso, Rubem Kalebe (2015).
 *
 * Valores constantes que representam os tokens da linguagem C+-.
 */

#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "tokens.h"
#include "eat_operators.h"

extern token_atual;

extern void eat(int var);
extern void eat_aux(int var);
extern void erro(char *s);
extern void LIST_STMTS();

void EXPR_ARITMETICA(){
    switch(token_atual){
        case LITERAL:
            eat_aux(LITERAL);
            if(token_atual == OP_ARITMETICO){
                eat_aux(OP_ARITMETICO);
                //OPR_ARIT();
                EXPR_ARITMETICA();
            }
            /*if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION);
                //OPR_ARIT();
                EXPR_ARITMETICA();
            }*/
        break;
        case IDENTIFIER:
            eat(IDENTIFIER);
            if(token_atual == OP_ARITMETICO){
                eat_aux(OP_ARITMETICO);
                //OPR_ARIT();
                EXPR_ARITMETICA();
            }
            /*if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION);
                //OPR_ARIT();
                EXPR_ARITMETICA();
            }*/
        break;
        default:
            erro("expressão");
    }
}

void EXPR_ARITMETICA_STMT2(){
    switch(token_atual){
        case COMMA:
            eat(COMMA);
            eat(IDENTIFIER);
            LIST_STMTS();
            break;
        case SEMICOLON:
            eat(SEMICOLON);
            //LIST_STMTS(); //STATMENT();
            break;
        default:
            erro("expression statment2");
    }
}

void EXPR_ARITMETICA_STMT(){
    switch(token_atual){
        case LITERAL:
            eat_aux(LITERAL);
            EXPR_ARITMETICA_STMT2();
        break;
        case IDENTIFIER:
            eat(IDENTIFIER);
            EXPR_ARITMETICA_STMT2();
        break;
        default:
            erro("expression statment1");
    }
}

void EXPR_COMP_LIST() {
    switch(token_atual) {// 'token_atual' é uma variável global
        case IDENTIFIER:
            EXPR_ARITMETICA();

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        case LITERAL:
            EXPR_ARITMETICA();

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        case TRUE:
            eat(TRUE);

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        case FALSE:
            eat(FALSE);

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        default: erro("expressão comparativa");
    }
}

void EXPR_COMP() {
    switch(token_atual) {// 'token_atual' é uma variável global
        case IDENTIFIER:
            EXPR_ARITMETICA(); eat_aux(OP_COMPARASSION); EXPR_ARITMETICA();

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        case LITERAL:
            EXPR_ARITMETICA(); eat_aux(OP_COMPARASSION); EXPR_ARITMETICA();

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        case TRUE:
            eat(TRUE);

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        case FALSE:
            eat(FALSE);

            if(token_atual == OP_COMPARASSION){
                eat_aux(OP_COMPARASSION); EXPR_COMP_LIST();
            }
        break;
        default: erro("expressão comparativa");
    }
}

#endif // EXPRESSION_H