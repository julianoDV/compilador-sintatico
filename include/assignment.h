/*
 * tokens.h
 *	created by Douglas Braz, Graco Babeuf, Juliano Cardoso, Rubem Kalebe (2015).
 *
 * Valores constantes que representam os tokens da linguagem C+-.
 */

#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include "tokens.h"
#include "expression.h"

extern token_atual;

extern void eat();
extern void eat_aux();
extern void erro(char *s);
extern void ACESSO_CAMPO();
extern void ACESSO_ARRAY();

void TEST_ATRIB_STMT(){
    switch(token_atual){
        case DOT:
            ACESSO_CAMPO(); eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA();
        break;
        case LBRACE:
            ACESSO_ARRAY(); eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA();
        break;
        default:
            erro("atrib statment");
    }
}

void ATRIBUICAO(){
    switch(token_atual){
        case TIPO:
            eat_aux(TIPO); eat(IDENTIFIER);

            //if(token_atual != OP_ASSIGNMENT){
              //  TEST_ATRIB_STMT();/* verifica se há acesso a um campo (estrutura)
                //depois faz uma atribuição */
            //}else{
            if(token_atual == COMMA){
                eat(COMMA);
                break;
            }else if(token_atual == OP_ASSIGNMENT){
                eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA();
                if(token_atual == COMMA){
                    eat(COMMA);
                    ATRIBUICAO();
                }
            }else{
                break;
            }
            //}

            /*if(token_atual == COMMA){
                eat(COMMA); ATRIBUICAO();
            }*/
            //eat(SEMICOLON);
        break;
        case IDENTIFIER:
            eat(IDENTIFIER);

            //if(token_atual != OP_ASSIGNMENT){
              //  TEST_ATRIB_STMT();/* verifica se há acesso a um campo (estrutura)
                //depois faz uma atribuição */
            //}else{
            if(token_atual == COMMA){
                eat(COMMA);
                ATRIBUICAO();
                break;
            }else if(token_atual == OP_ASSIGNMENT){
                eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA();
                if(token_atual == COMMA){
                    eat(COMMA);
                    ATRIBUICAO();
                }
            }else{
                break;
            }
            //}

            /*if(token_atual == COMMA){
                eat(COMMA); ATRIBUICAO();
            }*/
        break;
        case OP_ASSIGNMENT:
            eat(OP_ASSIGNMENT);
            EXPR_ARITMETICA();
            if(token_atual == COMMA){
                eat(COMMA); ATRIBUICAO();
            }
        break;
        default:
                erro("atribuição");
    }
}

void ATRIB_STMT(){
    switch(token_atual){
        case OP_ASSIGNMENT:
            eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA_STMT();// retirado -> LIST_STMTS();
        break;
        case DOT:
            ACESSO_CAMPO(); eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA();
        break;
        case LBRACE:
            ACESSO_ARRAY(); eat_aux(OP_ASSIGNMENT); EXPR_ARITMETICA();
        break;
        case SEMICOLON:
            eat(SEMICOLON);
        break;
        default:
            erro("atrib statment");
    }
}

#endif // ASSIGNMENT_H