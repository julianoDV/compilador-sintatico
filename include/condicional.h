#ifndef CONDICIONAL_H
#define CONDICIONAL_H

#include "expression.h"

extern void STATMENT();
extern void eat();
extern void erro(char *s);

extern token_atual;

void ELSE_STMT(){
    while(token_atual != ENDIF){
        STATMENT();
    }
}

void ELSIF_STMT(){
    while(token_atual != ELSE && token_atual != ELSIF){
        STATMENT();
    }
}

void IF_STMT(){
    switch(token_atual){// 'token_atual' é uma variável global
        case ELSIF:
            eat(ELSIF); eat(LPAREN); EXPR_COMP(); eat(RPAREN);
            STATMENT();

            if(token_atual != ELSE && token_atual != ELSIF)
                ELSIF_STMT();
            IF_STMT();
        break;
        case ELSE:
            eat(ELSE); STATMENT();

            if(token_atual != ENDIF)
                ELSE_STMT();
            eat(ENDIF);
        break;
        case ENDIF:
            eat(ENDIF);
        break;
        default :
            erro("if statment");
    }
}

#endif // CONDICIONAL_H