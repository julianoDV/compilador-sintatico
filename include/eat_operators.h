#ifndef EAT_OPERATORS_H
#define EAT_OPERATORS_H

extern void eat();
extern void erro(char *s);

extern token_atual;

void OPR_COMP(){
    switch(token_atual) {// 'token_atual' é uma variável global
        case EQ:
            eat(EQ);
        break;
        case DIFF:
            eat(DIFF);
        break;
        case LET:
            eat(LET);
        break;
        case GET:
            eat(GET);
        break;
        case LT:
            eat(LT);
        break;
        case GT:
            eat(GT);
        break;
        case AND:
            eat(AND);
        break;
        case OR:
            eat(OR);
        break;
        case NOT:
            eat(NOT);
        break;
        case SCAND:
            eat(SCAND);
        break;
        case SCOR:
            eat(SCOR);
        break;
        case AMPERSAND:
            eat(AMPERSAND);
        break;
        case BOR:
            eat(BOR);
        break;
          default: erro("opr_comp");
    }
}

void LITERAL_TYPE(){
    switch(token_atual){
        case DECIMAL:
            eat(DECIMAL);
        break;
        case REAL:
            eat(REAL);
        break;
        default:
            erro("literal type");
    }
}


void TYPE(){
    switch(token_atual){
        case INT:
            eat(INT);
        break;
        case FLOAT:
            eat(FLOAT);
        break;
        case DOUBLE:
            eat(DOUBLE);
        break;
        case SHORT:
            eat(SHORT);
        break;
        case LONG:
            eat(LONG);
        break;
        case OCTAL:
            eat(OCTAL);
        break;
        case HEX:
            eat(HEX);
        break;
        case BYTE:
            eat(BYTE);
        break;
        case BOOL:
            eat(BOOL);
        break;
        default :
            erro("tipo");
            break;
    }
}

void OPR_ARIT(){
    switch(token_atual){
        case PLUS:
            eat(PLUS);
        break;
        case MINUS:
            eat(MINUS);
        break;
        case ASTERISK:
            eat(ASTERISK);
        break;
        case DIV:
            eat(DIV);
        break;
        case MOD:
            eat(MOD);
        break;
        default:
            erro("operador aritmético");
    }
}

void OPR_ASS(){
    switch(token_atual){
        case NORMALASSIGN:
            eat(NORMALASSIGN);
        break;
        case PLUSASSIGN:
            eat(PLUSASSIGN);
        break;
        case MINUSASSIGN:
            eat(MINUSASSIGN);
        break;
        case PRODUCTASSIGN:
            eat(PRODUCTASSIGN);
        break;
        case DIVASSIGN:
            eat(DIVASSIGN);
        break;
        case MODASSIGN:
            eat(MODASSIGN);
        break;
        case BORASSIGN:
            eat(BORASSIGN);
        break;
        case BANDASSIGN:
            eat(BANDASSIGN);
        break;
        case BOREXCASSIGN:
            eat(BOREXCASSIGN);
        break;
        case LSHIFTASSIGN:
            eat(LSHIFTASSIGN);
        break;
        case RSHIFTASSIGN:
            eat(RSHIFTASSIGN);
        break;
        default:
            erro("opr_ass");
    }
}

#endif //EAT_OPERATORS_H