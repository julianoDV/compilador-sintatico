/*
 * sintatico.c
 *	created by Douglas Braz, Graco Babeuf, Juliano Cardoso, Rubem Kalebe (2015).
 *
 * Implementacao do scanner.
 */

#include <stdio.h>
#include <stdlib.h>
#include "tokens.h"
#include "expression.h"
#include "assignment.h"
#include "eat_operators.h"
#include "condicional.h"

// FUNÇÕES LOCAIS

void yyerror(char *);
void advance();
void eat(int t);
void eat_aux(int var);
void PROGRAM();
void METHOD();
void BLOCO_PRINCIPAL();
void STATMENTS();
void STATMENT();
void LADO_ESQ_ATRIB();
void ACESSO_CAMPO();
void ACESSO_ARRAY();
void FOR_STMT();
void WHILE_STMT();
void FUNC_STMT();
void IF_BODY();
void LIST_STMTS();
void DECLARACAO();
void ARGUMENTS();

void erro(char *S);
//char* getTokenName(int value);

//FUNÇÕES EXTERNAS
extern int yylex();
extern int yywrap();

// VARIÁVEIS GLOBAIS LOCAIS
int token_atual;
int token_anterior;

//VARIÁVEIS EXTERNAS
extern int yylineno;
extern char* yytext;
extern FILE* yyin;
extern int AUX_TYPE;
extern int AUX_OP_ARITMETICO;
extern int AUX_LITERAL;
extern int AUX_OP_COMPARASSION;
extern int AUX_OP_ASSIGNMENT;

int main(int argc, char *argv[]) {

	int vtoken;

	if (argc > 0) {
		yyin = fopen(argv[1], "r");
	} else {
		yyin = stdin;
	}

    advance();// Captura o primeiro símbolo do código.
    PROGRAM();
	printf("Saindo do compilador G+- ... PASSOU SEM ERROS\n");
	return 0;
}

void yyerror(char *s) {
	fprintf(stderr, "!!! O símbolo %s não é reconhecido pela linguagem C+-: %s na linha %i\n",yytext, s, yylineno);
}

void advance(){
	token_atual = yylex();
}

void eat(int token_esperado){
	if(token_atual == token_esperado){
        token_anterior = token_atual;
	    printf("encontrado o token %s\n",yytext);
	    advance();
	}else{
	    erro("não passou");
	    return;
	}
}

void eat_aux(int tk_generico){
    switch(tk_generico){
        case TIPO:
            token_atual = AUX_TYPE;
            AUX_TYPE = 0;
            TYPE();
        break;
        case OP_ARITMETICO:
            token_atual = AUX_OP_ARITMETICO;//token_atual recebe o valor verdadeiro a ser consumido
            AUX_OP_ARITMETICO = 0;
            OPR_ARIT();
        break;
        case OP_COMPARASSION:
            token_atual = AUX_OP_COMPARASSION;
            AUX_OP_COMPARASSION = 0;
            OPR_COMP();
        break;
        case OP_ASSIGNMENT:
            token_atual = AUX_OP_ASSIGNMENT;
            AUX_OP_ASSIGNMENT = 0;
            OPR_ASS();
        break;
        case LITERAL:
            token_atual = AUX_LITERAL;
            AUX_LITERAL = 0;
            LITERAL_TYPE();
        break;
        default:
            erro("eat auxiliar");
    }
}

void PROGRAM(){
        switch(token_atual){
            case IMPORT:// library declarations->library declaration
                eat(IMPORT); eat(STR); eat(SEMICOLON); PROGRAM();
            break;
            case PROC:
                eat(PROC); METHOD();
            break;
            case FUNC:
                eat(FUNC); eat_aux(TIPO); eat(IDENTIFIER);
                eat(LPAREN); ARGUMENTS(); eat(RPAREN);
                FUNC_STMT();

                eat(RETURN);

                if(token_atual == IDENTIFIER){
                    EXPR_ARITMETICA();
                }else if(token_atual == LITERAL){
                    EXPR_ARITMETICA();
                }else if(token_atual == TRUE){
                    eat(TRUE);
                }else{
                    eat(FALSE);
                }

                eat(SEMICOLON);

                eat(ENDFUNC);
                PROGRAM();
            break;
            case TIPO://variable declarators
                DECLARACAO(); PROGRAM(); //eat_aux(TIPO); eat(IDENTIFIER); LIST_STMTS(); PROGRAM();
            break;
            case END_OF_FILE:
                break;
            default :
                erro("programa");
        }
}

void METHOD(){
    switch(token_atual){
        case MAIN:
            BLOCO_PRINCIPAL(); eat(ENDPROC);
        break;
        case IDENTIFIER:
            eat(IDENTIFIER); eat(LPAREN); ARGUMENTS(); eat(RPAREN); eat(ENDPROC); PROGRAM();
        break;
        default:
            erro("processo");
            break;
    }
}

void BLOCO_PRINCIPAL(){
	switch(token_atual){
		case MAIN:
			eat(MAIN); eat(LPAREN); eat(RPAREN); STATMENTS();
		break;
		default: erro("bloco principal");
		break;
	}
}

void STATMENTS(){
	STATMENT();
    if(token_atual != ENDPROC){
        STATMENTS();
    }
}

void STATMENT(){
    switch(token_atual){
	case IDENTIFIER:
        DECLARACAO();
	    /* 2° eat(IDENTIFIER);
        if(token_atual == SEMICOLON){
            eat(SEMICOLON);
        }else{
            ATRIBUICAO(); eat(SEMICOLON);
        }*/
        // 1° eat(IDENTIFIER); LIST_STMTS();
    break;
	case IF:
        eat(IF); eat(LPAREN); EXPR_COMP(); eat(RPAREN);
        //STATMENTS();
	   if(token_atual != ELSIF && token_atual != ELSE && token_atual != ENDIF)
       		IF_BODY();
        IF_STMT();
    break;
    case FOR:
            eat(FOR); eat(LPAREN);
            if(token_atual == TIPO)
                ATRIBUICAO();
            else
                erro("atribuição no laço 'FOR' ");

            eat(SEMICOLON); EXPR_COMP(); eat(SEMICOLON); ATRIBUICAO(); eat(RPAREN);
                //STATMENT();
                if(token_atual != ENDFOR)
                    FOR_STMT();
            eat(ENDFOR);
    break;
    case WHILE:
            eat(WHILE); eat(LPAREN); EXPR_COMP(); eat(RPAREN);
                if(token_atual != ENDWHILE)
                    WHILE_STMT();
            eat(ENDWHILE);
    break;
    case TIPO:
         eat_aux(TIPO); STATMENT();// eat(IDENTIFIER); eat(SEMICOLON);
    break;
    case RETURN:
        eat(RETURN); EXPR_ARITMETICA();
    break;
    default:
        erro("statment");
    }
}

void FOR_STMT(){
    while(token_atual != ENDFOR){
        STATMENT();
    }
}

void WHILE_STMT(){
    while(token_atual != ENDWHILE){
        STATMENT();
    }
}

void IF_BODY(){
    while(token_atual != ELSIF && token_atual != ELSE && token_atual != ENDIF){
        STATMENT();
    }
}

void FUNC_STMT(){
    while(token_atual != RETURN){
        STATMENT();
    }
}

void LIST_STMTS(){
	switch(token_atual){
		case SEMICOLON:
			eat(SEMICOLON);
		break;
		case COMMA:
			eat(COMMA);
			eat(IDENTIFIER);
			LIST_STMTS();
		break;
		case OP_ASSIGNMENT:
			ATRIB_STMT();
        break;
		default:
			erro("list statments");
	}

}

void DECLARACAO(){
	switch(token_atual){
		case TIPO:
			eat_aux(TIPO);
			eat(IDENTIFIER);
			if(token_atual == COMMA){
				eat(COMMA);
				DECLARACAO();
			}else if(token_atual == OP_ASSIGNMENT){
                ATRIBUICAO();
                eat(SEMICOLON);
                //DECLARACAO();
            }else{
                eat(SEMICOLON);
            }
		break;
        case IDENTIFIER:
            eat(IDENTIFIER);

            if(token_atual == COMMA){
                eat(COMMA);
                DECLARACAO();
            }else if(token_atual == OP_ASSIGNMENT){
                ATRIBUICAO();
                eat(SEMICOLON);
                //DECLARACAO();
            }else{
                eat(SEMICOLON);
            }
        break;
		default:
			erro("declaracao");
			break;

	}
}

void ARGUMENTS(){
    switch(token_atual){
        case TIPO:
            eat_aux(TIPO); eat(IDENTIFIER);

            if(token_atual == COMMA){
                eat(COMMA);
                ARGUMENTS();
            }
        break;
        default:
            erro("argumentos");
    }
}

void ACESSO_CAMPO(){
	switch(token_atual){
		case DOT:
			eat(DOT); eat(IDENTIFIER);
            if(token_atual == DOT)
                ACESSO_CAMPO();
		break;
		default :
			erro("acesso campo");
	}
}

void ACESSO_ARRAY(){
	switch(token_atual){
		case LBRACE:
			eat(LBRACE); eat(RBRACE);
		break;
		default :
			erro("acesso array");
	}
}

void erro(char *s){// Imprime uma mensagem de erro e finaliza a compilação
	printf("erro: '%s' na linha %i\n",s,yylineno); exit(0);
}

/*
char* getTokenName(int value) {
	switch(value) {
		case DECIMAL: return "DECIMAL";
		case IDENTIFIER: return "IDENTIFIER";
		case REAL: return "REAL";
		case TRUE: return "TRUE";
		case FALSE: return "FALSE";
		case STR: return "STR";
		case HEX: return "HEX";
		case OCTAL: return "OCTAL";
		case BYTE: return "BYTE";
		case BOOL: return "BOOL";
		case SHORT: return "SHORT";
		case INT: return "INT";
		case LONG: return "LONG";
		case FLOAT: return "FLOAT";
		case DOUBLE: return "DOUBLE";
		case CHAR: return "CHAR";
		case STRING: return "STRING";
		case STRUCT: return "STRUCT";
		case UNION: return "UNION";
		case ENUM: return "ENUM";
		case AUTO: return "AUTO";
		case IF: return "IF";
		case ELSIF: return "ELSIF";
		case ELSE: return "ELSE";
		case WHILE: return "WHILE";
		case FOR: return "FOR";
		case RETURN: return "RETURN";
		case PROC: return "PROC";
		case FUNC: return "FUNC";
		case ENDBLOCK: return "ENDBLOCK";
		case ENDCASE: return "ENDCASE";
		case ENDENUM: return "ENDENUM";
		case ENDFOR: return "ENDFOR";
		case ENDFUNC: return "ENDFUNC";
		case ENDIF: return "ENDIF";
		case ENDPROC: return "ENDPROC";
		case ENDSTRUCT: return "ENDSTRUCT";
		case ENDUNION: return "ENDUNION";
		case ENDWHILE: return "ENDWHILE";
		case BREAK: return "BREAK";
		case CONT: return "CONT";
		case DO: return "DO";
		case CASE: return "CASE";
		case WHEN: return "WHEN";
		case CONST: return "CONST";
		case STATIC: return "STATIC";
		case SIZEOF: return "SIZEOF";
		case TYPEOF: return "TYPEOF";
		case PRINT: return "PRINT";
		case PRINTLN: return "PRINTLN";
		case READ: return "READ";
		case NNULL: return "NNULL";
		case NORMALASSIGN: return "NORMALASSIGN";
		case LT: return "LT";
		case GT: return "GT";
		case LET: return "LET";
		case GET: return "GET";
		case EQ: return "EQ";
		case DIFF: return "DIFF";
		case PLUS: return "PLUS";
		case MINUS: return "MINUS";
		case ASTERISK: return "ASTERISK";
		case DIV: return "DIV";
		case MOD: return "MOD";
		case PLUSASSIGN: return "PLUSASSIGN";
		case MINUSASSIGN: return "MINUSASSIGN";
		case PRODUCTASSIGN: return "PRODUCTASSIGN";
		case DIVASSIGN: return "DIVASSIGN";
		case MODASSIGN: return "MODASSIGN";
		case NOT: return "NOT";
		case AND: return "AND";
		case OR: return "OR";
		case SCAND: return "SCAND";
		case SCOR: return "SCOR";
		case COMPLEMENT: return "COMPLEMENT";
		case AMPERSAND: return "AMPERSAND";
		case BANDASSIGN: return "BANDASSIGN";
		case BOR: return "BOR";
		case BORASSIGN: return "BORASSIGN";
		case BOREXC: return "BOREXC";
		case BOREXCASSIGN: return "BOREXCASSIGN";
		case LSHIFT: return "LSHIFT";
		case LSHIFTASSIGN: return "LSHIFTASSIGN";
		case RSHIFT: return "RSHIFT";
		case RSHIFTASSIGN: return "RSHIFTASSIGN";
		case SEMICOLON: return "SEMICOLON";
		case DOT: return "DOT";
		case COMMA: return "COMMA";
		case LPAREN: return "LPAREN";
		case RPAREN: return "RPAREN";
		case LSQPAREN: return "LSQPAREN";
		case RSQPAREN: return "RSQPAREN";
		case LBRACE: return "LBRACE";
		case RBRACE: return "RBRACE";
        case MAIN: return "MAIN";
	}
}*/
