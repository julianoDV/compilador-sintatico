/*
 * g+-.l
 *	created by Douglas Braz, Graco Babeuf, Juliano Cardoso, Rubem Kalebe (2015).
 *
 * Especificacoes lex da linguagem.
 */

%{
#include <stdio.h>
#include <stdlib.h>
#include "tokens.h"

int yylval;

int yylineno;

int AUX_TYPE = 0;
int AUX_OP_ARITMETICO = 0;
int AUX_LITERAL = 0;
int AUX_OP_COMPARASSION = 0;
int AUX_OP_ASSIGNMENT = 0;

int	octal(char *str);

int	hex(char *str);

// void multLineComment();

//void lineComment();
%}

%%

"if"				return IF;
"elsif"				return ELSIF;
"else"				return ELSE;
"while"				return WHILE;
"for"				return FOR;
"main"				return MAIN;
"return"			return RETURN;
"procedure"			return PROC;
"function"			return FUNC;
"endblock"			return ENDBLOCK;
"endcase"			return ENDCASE;
"endenum"			return ENDENUM;
"endfor"			return ENDFOR;
"endfunction"		return ENDFUNC;
"endif"				return ENDIF;
"endprocedure"		return ENDPROC;
"endstruct"			return ENDSTRUCT;
"endunion"			return ENDUNION;
"endwhile"			return ENDWHILE;
"break"				return BREAK;
"continue"			return CONT;
"do"				return DO;
"case"				return CASE;
"when"				return WHEN;
"const"				return CONST;
"static"			return STATIC;
"sizeof"			return SIZEOF;
"typeof"			return TYPEOF;
"print"				return PRINT;
"println"			return PRINTLN;
"read"				return READ;
"null"				return NNULL;
"import"			return IMPORT;

"true"				return TRUE;
"false"				return FALSE;

"auto"				{ AUX_TYPE = AUTO; return TIPO;}
"bool"				{ AUX_TYPE = BOOL; return TIPO;}
"byte"				{ AUX_TYPE = BYTE; return TIPO;}
"char"				{ AUX_TYPE = CHAR; return TIPO;}
"double"			{ AUX_TYPE = DOUBLE; return TIPO;}
"enum"				{ AUX_TYPE = ENUM; return TIPO;}
"float"				{ AUX_TYPE = FLOAT; return TIPO;}
"int"				{ AUX_TYPE = INT; return TIPO;}
"long"				{ AUX_TYPE = LONG; return TIPO;}
"short"				{ AUX_TYPE = SHORT; return TIPO;}
"string"			{ AUX_TYPE = STRING; return TIPO;}
"struct"			{ AUX_TYPE = STRUCT; return TIPO;}
"union"				{ AUX_TYPE = UNION; return TIPO;}

"("					return LPAREN;
")"					return RPAREN;
"["					return LSQPAREN;
"]"					return RSQPAREN;
"{"					return LBRACE;
"}"					return RBRACE;

";"					return SEMICOLON;
"."					return DOT;
","					return COMMA;

"<"					{ AUX_OP_COMPARASSION = LT; return OP_COMPARASSION; }
">"					{ AUX_OP_COMPARASSION = GT; return OP_COMPARASSION; }
"<="				{ AUX_OP_COMPARASSION = LET; return OP_COMPARASSION; }
">="				{ AUX_OP_COMPARASSION = GET; return OP_COMPARASSION; }
"=="				{ AUX_OP_COMPARASSION = EQ; return OP_COMPARASSION; }
"!="				{ AUX_OP_COMPARASSION = DIFF; return OP_COMPARASSION; }

"+"					{ AUX_OP_ARITMETICO = PLUS; return OP_ARITMETICO; }
"-"					{ AUX_OP_ARITMETICO = MINUS; return OP_ARITMETICO; }
"*"					{ AUX_OP_ARITMETICO = ASTERISK; return OP_ARITMETICO; }
"/"					{ AUX_OP_ARITMETICO = DIV; return OP_ARITMETICO; }
"%"					{ AUX_OP_ARITMETICO = MOD; return OP_ARITMETICO; }

"="					{ AUX_OP_ASSIGNMENT = NORMALASSIGN; return OP_ASSIGNMENT; }
"+="				{ AUX_OP_ASSIGNMENT = PLUSASSIGN; return OP_ASSIGNMENT; }
"-="				{ AUX_OP_ASSIGNMENT = MINUSASSIGN; return OP_ASSIGNMENT; }
"*="				{ AUX_OP_ASSIGNMENT = PRODUCTASSIGN; return OP_ASSIGNMENT; }
"/="				{ AUX_OP_ASSIGNMENT = DIVASSIGN; return OP_ASSIGNMENT; }
"%="				{ AUX_OP_ASSIGNMENT = MODASSIGN; return OP_ASSIGNMENT; }
"|="				{ AUX_OP_ASSIGNMENT = BORASSIGN; return OP_ASSIGNMENT; }
"&="				{ AUX_OP_ASSIGNMENT = BANDASSIGN; return OP_ASSIGNMENT; }
"^="				{ AUX_OP_ASSIGNMENT = BOREXCASSIGN; return OP_ASSIGNMENT; }
"<<="				{ AUX_OP_ASSIGNMENT = LSHIFTASSIGN; return OP_ASSIGNMENT; }
">>="				{ AUX_OP_ASSIGNMENT = RSHIFTASSIGN; return OP_ASSIGNMENT; }

"!"					{ AUX_OP_COMPARASSION = NOT; return OP_COMPARASSION; }
"&&"				{ AUX_OP_COMPARASSION = AND; return OP_COMPARASSION; }
"||"				{ AUX_OP_COMPARASSION = OR; return OP_COMPARASSION; }
"&&&"				{ AUX_OP_COMPARASSION = SCAND; return OP_COMPARASSION; }
"|||"				{ AUX_OP_COMPARASSION = SCOR; return OP_COMPARASSION; }
"~"					return COMPLEMENT;
"&"					{ AUX_OP_COMPARASSION = AMPERSAND; return OP_COMPARASSION; }
"|"					{ AUX_OP_COMPARASSION = BOR; return OP_COMPARASSION; }

"^"					return BOREXC;
"<<"				return LSHIFT;
">>"				return RSHIFT;
<<EOF>>				return END_OF_FILE;

[_a-zA-Z][_a-zA-Z0-9]* {
	yylval = *yytext;
	return IDENTIFIER;
}

[+-]?0[0-7]+ {
	yylval = octal(yytext);
	return OCTAL;
}

[0-9]+	{
	yylval = atoi(yytext);
	AUX_LITERAL = DECIMAL;
	return LITERAL;
}

[+-]?0[xX][0-9a-fA-F]+ {
	yylval = hex(yytext);
	return HEX;
}

[0-9]+([.][0-9]+)?([eE][+-]?[0-9]+)? {
    yylval = atof(yytext);
    AUX_LITERAL = REAL;
    return LITERAL;
}

\"(\\.|[^"])*\" {
	/* Uma string começa com '"' seguida de qualquer numero de caracteres que nao sao '"'
		ou sao uma \ seguida de um caractere, e termina com '"' */
	yylval = *yytext;
	return STR;
}

\/\/		{ /* do nothing \/\/[^\n]*\n  */ }

[ \t]*		{ /* do nothing */ }

[\n]		{ yylineno = yylineno + 1; }

\/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\/ { /* do nothing */ }

. 			{ fprintf(stderr, "Símbolo inválido '%s' na linha %i\n", yytext, yylineno); exit(0);}
%%

int yywrap(void){
	return 1;
}

/* Rotinas para conversao de numeros */

int octal(char *str) {
	int num;
	(void) sscanf(str, "%o", &num);
	return num;
}

int hex(char *str) {
	int num;
	(void) sscanf(str+2, "%x", &num);
	return num;
}

/* Rotinas para ignorar comentarios */

/* void multLineComment() {
	char c;
loop:
	while((c = input()) != '*' && c != 0)
		;
	if((c = input()) != '/' && c != 0) {
		goto loop;
	}
}

void lineComment() {
	char c;
	while((c = input()) != '\n' && c != 0)
		;
} */

/*
 [+-]?[0-9]+[_a-zA-Z0-9]+ {
	fprintf(stderr, "Incorrect integer or identifier '%s'.\n", yytext);
}
*/